import scala.swing._

object Main {
  def main(args: Array[String]) {
    Swing.onEDT(initGUI) //not entirely sure what this is
  }
  
  import java.io.FileWriter

  
  var savedText = ""
  var isFilePath = false
  def initGUI() {
    val sMain = new Dimension(1024, 768)
    
    
    val textPane = new EditorPane {preferredSize = sMain}
    val scrollPane = new ScrollPane (textPane)
                        //scrollPane.verticalScrollBar.visible = Always                                                        
    
    
    val testButton = new Button { text = "test button" }
    val fileButton = new Button { text = "File Chooser"        }
    
    val fileChooser = new FileChooser()
    val savePanel = new BoxPanel(Orientation.Vertical) {} 
    
    
    val mainFrame = new MainFrame       { 
      title = "LK-Text" 
      preferredSize = sMain
      minimumSize = new Dimension (150, 200)
      menuBar = new MenuBar{
      
      
        def saveAs(){
          var fileValue = fileChooser.showSaveDialog(savePanel)
          if (fileValue.toString == "Approve"){
            //check if file exists, ask to overwrite
            savedText = textPane.text
            var openFile = new FileWriter(fileChooser.selectedFile, false)  // true for append
            try {
              openFile.write(savedText)
            } finally {
               openFile.close()
               println("file saved")
               isFilePath = true
            }
          }
            else{
              println("file not saved")
            }
          }
      
      
        contents+= new Menu("File") {
          contents += new MenuItem (Action("Open File") {
            var fileValue = fileChooser.showOpenDialog(savePanel)
            if (fileValue.toString == "Approve"){
              textPane.text = scala.io.Source.fromFile(fileChooser.selectedFile, "utf-8").mkString
              isFilePath = true
              repaint()
            }
            else{
              println("Cancled")
            }
          })
          contents += new MenuItem (Action("Save") {                                                
            if (isFilePath) { 
              savedText = textPane.text
              var openFile = new FileWriter(fileChooser.selectedFile, false)  // true for append
              try {
                openFile.write(savedText)
              } finally {
                 openFile.close()
                 println("file saved")
              }
            }
            
            else{
            saveAs()
              
            }
          })
          
          contents += new MenuItem (Action("Save As") {
            saveAs()
          })
          contents += new MenuItem(Action("quit") {
            sys.exit(0) 
          })
        }
      }
    }
    val uslPanel = new BoxPanel(Orientation.Vertical) {
      //contents += textPane
      contents += scrollPane
//      contents += testButton // -- test buttons for below the editor pane
//      contents += fileButton
    }
    val rTestButton = new Reactor {}
    rTestButton.listenTo(testButton)
    rTestButton.reactions += {
      case event.ButtonClicked(_) => println(textPane.text = "1234")
    }
    val rFileButton = new Reactor {}
    rFileButton.listenTo(fileButton)
    rFileButton.reactions +={
      case event.ButtonClicked(_) => fileChooser.showSaveDialog(savePanel)
      uslPanel.repaint()
    }

    mainFrame.contents = uslPanel
    mainFrame.visible = true
        
    
  }
 
}
    
