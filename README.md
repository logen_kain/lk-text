LK-Text is the first step to creating Logen Kain's desktop content management software for publishing his blogs.  Everything released as part of this project will be licensed with the wonderful Internet Systems Consortium license.

LK-Text is only one part of the suite which will eventually include writing, editing, and publishing a personal blog via ssh.
